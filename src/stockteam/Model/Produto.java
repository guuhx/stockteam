package stockteam.Model;

public class Produto {
    private int codigo;
    private String nome;

    public Produto(int codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    public Produto(){

    }

    public int getCodigo() {
        return codigo;
    }

    public String getNome() {
        return nome;
    }
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
